:: Helper class will generate test data wherever you point it at.
:: 2nd and 3d arguments are array length and max value respectively
java -cp ./target/homework-1.0-SNAPSHOT.jar com.company.homework.Helper ./src/test/resources/input.txt 32000 32000
:: Run volume meter
java -jar ./target/homework-1.0-SNAPSHOT.jar ./src/test/resources/input.txt