package com.company.homework;

public enum IntConstants {
    MAX_ARRAY_LENGTH(32_000), MIN_ELEMENT_VALUE(0), MAX_ELEMENT_VALUE(32_000);

    int value;

    IntConstants(int value) {
        this.value = value;
    }
}
