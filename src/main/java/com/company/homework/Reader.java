package com.company.homework;

import java.io.*;
import java.util.StringTokenizer;

/**Handy utility for home projects
 * */
public class Reader implements AutoCloseable {
    // java.nio.charset.Charset cs = StandardCharsets.UTF_8; // pass to io stream constructor
    private BufferedReader in;
    private StringTokenizer curLine;
    private String delimiter;

    public Reader(String inputFileName, String delimiter) throws IOException {
        this.delimiter = delimiter;
        InputStream is = System.in;

        if (is.available() == 0) {
            is = new FileInputStream(inputFileName);
        }
        in = new BufferedReader(new InputStreamReader(is));
    }

    @Override
    public void close() throws IOException {
        in.close();
    }

    /**
     * @return false if EOF
     */
    public boolean fetchNextLine() throws IOException {
        String next = in.readLine();
        if (next == null) {
            curLine = null;
            return false;
        }
        curLine = new StringTokenizer(next, delimiter, false);
        return true;
    }

    public boolean hasNextToken() {
        return curLine != null && curLine.hasMoreTokens();
    }

    public String nextString() {
        return curLine.nextToken();
    }
}
