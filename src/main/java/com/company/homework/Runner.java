package com.company.homework;

import java.io.IOException;

import static com.company.homework.IntConstants.*;

public class Runner {
    public static void main(String... args) {
        try (Reader reader = new Reader(args[0], " ")) {
            Logic logic = new Logic();
            int elementCounter = 0;
            reader.fetchNextLine();
            while (reader.hasNextToken()) {
                if (++elementCounter > MAX_ARRAY_LENGTH.value)
                    throw new IllegalArgumentException("Input array is larger than " + MAX_ARRAY_LENGTH.value);
                logic.accept(validateInput(reader.nextString()));
            }
            System.out.println(logic.getResult().toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static int validateInput(String strVal) {
        try {
            int value = Integer.valueOf(strVal);
            if (value < MIN_ELEMENT_VALUE.value || value > MAX_ELEMENT_VALUE.value) {
                throw new IllegalArgumentException("Input value " + strVal + " is not between " +
                        MIN_ELEMENT_VALUE.value + " and " + MAX_ELEMENT_VALUE.value);
            }
            return value;
        } catch (NumberFormatException e) {
            RuntimeException ne = new NumberFormatException("Invalid array element: non-int value '" + strVal + '\'');
            ne.initCause(e);
            throw ne;
        }
    }
}