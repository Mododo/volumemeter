package com.company.homework;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;

/**Streaming implementation of given task with O(N) amortised time complexity and O(N) additional space requirement.
 * Just to show that array may be processed while it is being read, though its response time is not stable.
 * It is not thread-safe anyway, because it was not required in task
 * (needs to partition input, process each part independently, append their stacks and merge sequentially).
 * */
class Logic {
    private int max = 0; // highest value among all passed
    private int prev = 0; // last passed array element
    private int volume = 0; // may use long or BigInteger for larger streams; 32_000² < Integer.MAX_VALUE = 2³¹-1

    // Stack to step backwards while collapsing
    // After all input array is processed, stack will contain a sequence of heights,
    // that starts with first array element, than is non-decreasing up to global maximum,
    // and is non-increasing up to last element in the array.
    // Collapsing is the process of removing "gaps" between "peaks", counting their water capacity
    // In worst-case scenario, every input vertex is accessed twice - once in input array, and once in stack
    // (because either we can't step behind it, or we pop it from stack).
    private Deque<Node> stack = new LinkedList<>();

    // Local class to store in stack. Not usable outside this method.
    private static class Node{
        private int height;
        private int otherAmount;

        Node(int height) {
            this.height = height;
            this.otherAmount = 0;
        }
    }

    public void accept (int vertex) {
        if (vertex > prev){ // previous element was a "gap" (local minimum), so it can be filled with water
            Node curNode = new Node(vertex);
            int height = Math.min(vertex, max);
            while (!stack.isEmpty()) {
                Node head = stack.peek();
                // Do not step behind higher peak
                // Do not step behind highest passed peak (in case we faced one of equal height)
                if (head.height > vertex || head.height == max) break;
                // Estimate water capacity and remove element from stack
                int heightDiff = height - head.height;
                volume += heightDiff + heightDiff * head.otherAmount;
                curNode.otherAmount += head.otherAmount + 1;
                stack.pop();
            }
            stack.push(curNode);
            if (vertex > max) max = vertex;
        } else {
            // Preserve vertex for further collapsing
            stack.push(new Node(vertex));
        }
        prev = vertex;
    }

    Number getResult(){ // allows to return Long or BigInteger in future
        return volume;
    }

    static Number processStatelessly(int[] value) {
        Logic impl = new Logic();
        Arrays.stream(value).forEach(impl::accept);
        return impl.getResult();
    }
}