package com.company.homework;

import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

import static com.company.homework.IntConstants.*;

public class Helper {

    /**
     * Generates test data
     * Accepts file name, array size and max value, or uses the defaults
     */
    public static void main(String... args) {
        String outFile = args.length > 0 ? args[0] : "./src/test/resources/input.txt";
        int length = args.length > 0 ? Integer.valueOf(args[1]) : MAX_ARRAY_LENGTH.value;
        int maxValue =  args.length > 0 ? Integer.valueOf(args[2]) : MAX_ELEMENT_VALUE.value;

        try (PrintWriter out = new PrintWriter(new FileOutputStream(outFile))) {
            int[] data = generateTestData(length, maxValue);
            int result = naiveImpl(data);
            StringBuilder sb = new StringBuilder();
            for (int d : data) {
                sb.append(d).append(" ");
            }
            sb.append('\n').append(length).append(" // size of int array above, values between 0 and ").append(length);
            sb.append('\n').append(result).append(" // expected result");
            sb.append('\n').append("Only the first line is used");
            out.println(sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Generate test data
     */
    static int[] generateTestData() {
        return generateTestData(MAX_ARRAY_LENGTH.value, MAX_ELEMENT_VALUE.value);
    }

    private static int[] generateTestData(int arrayLength, int maxElementSize) {
        if (IntConstants.MIN_ELEMENT_VALUE.value != 0)
            throw new UnsupportedOperationException("Data generation with min value != 0 not implemented yet");
        int[] result = new int[arrayLength];
        Random rand = new Random();
        for (int i = 0; i < arrayLength; i++) {
            result[i] = rand.nextInt(maxElementSize);
        }
        return result;
    }

    /**
     * Naive implementation of given task with O(N²) time complexity and O(N) additional space
     * that sorts (copy of) input array to find local maximums
     * <p>
     * Used as a reference because of easy implementation
     */
    static int naiveImpl(int[] input) {
        class IndexedEntry {
            int value;
            int position;

            IndexedEntry(int value, int position) {
                this.value = value;
                this.position = position;
            }
        }
        // do not modify initial data
        int[] arr = input.clone();

        // build index, sorted by given array values (descending)
        IndexedEntry[] sorted = new IndexedEntry[arr.length];
        for (int i = 0; i < arr.length; i++) {
            sorted[i] = new IndexedEntry(arr[i], i);
        }
        Arrays.sort(sorted, Comparator.comparingInt(a -> -a.value));

        // estimate volume
        int volume = 0;
        for (int i = 0, j = 1; j < sorted.length; i++, j++) {
            final int leftBorder = Math.min(sorted[i].position, sorted[j].position);
            final int rightBorder = Math.max(sorted[i].position, sorted[j].position);
            final int height = sorted[j].value;

            // pass between pair of local maximums and fill gap with water
            for (int k = leftBorder; k < rightBorder; k++) {
                if (height > arr[k]) {
                    volume += height - arr[k];
                    arr[k] = height;
                }
            }
        }

        return volume;
    }
}
