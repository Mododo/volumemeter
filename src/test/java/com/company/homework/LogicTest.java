package com.company.homework;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.function.Function;

public class LogicTest {

    private void predefinedTestBody(Function<int[], Number> impl){
        Assertions.assertEquals(9, impl.apply(new int[]{5, 2, 3, 4, 5, 4, 0, 3, 1}),
                "Vanilla test from task");
        Assertions.assertEquals(0+46+0, impl.apply(new int[]{5, 11, 12, 7, 6, 6, 12, 8, 5, 6, 14, 4, 12, 14, 9}),
                "Expected value on predefined array");
        Assertions.assertEquals(0+46+0, impl.apply(new int[]{5, 11, 12, 6, 7, 6, 12, 6, 5, 8, 14, 12, 4, 14, 9}),
                "Permuted values between local maximums must not change the result");
        Assertions.assertEquals(6+46+5, impl.apply(new int[]{11, 5, 12, 6, 7, 6, 12, 6, 5, 8, 14, 12, 4, 9, 14}),
                "Permuted values on ends give new local maximums and change the result");
    }

    @Test
    public void testNaive(){
        predefinedTestBody(Helper::naiveImpl);
    }

    @Test
    public void testLogic(){
        predefinedTestBody(Logic::processStatelessly);
    }

    @Test
    public void testSanity(){
        int[] data = Helper.generateTestData();
        long start = System.currentTimeMillis();

        int naive = Helper.naiveImpl(data);
        long naiveDone = System.currentTimeMillis();

        int main = Logic.processStatelessly(data).intValue();
        long mainDone = System.currentTimeMillis();

        System.out.println("Naive impl took " + String.valueOf(naiveDone - start) + " ms");
        System.out.println("Main  impl took " + String.valueOf(mainDone - naiveDone) + " ms");

        Assertions.assertEquals(naive, main);
    }
}
